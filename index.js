// Bài tập 1: Tính tiền lương nhân viên
/**
 * Đầu vào
 *      luongMotNgay = 100000
 * Các bước xử lý
 *      b1: tạo biến cho lương 1 ngày, số ngày làm
 *      b2: sử dụng công thức songaylamValue * luongMotngay
 * Đầu ra
 *      số tiền ngày lương của nhân viên đã nhập
 */
var luongMotNgay = 100000;

function luongNv() {
  var soNgayLam = document.getElementById("soNgay");
  var songaylamValue = soNgayLam.value;
  console.log("luongnhanvien= ", songaylamValue * luongMotNgay);
}
// Kết thúc bài tập 1

// Bài tập 2: Tính giá trị trung bình
/**
 * Đầu vào
 *    đặt tên biến và gán giá trị cho các biến
 * Các bước xử lý
 *    sử dụng công thức: tính tổng các giá trị và chia 5
 * Đầu ra
 *    result = 3
 */
var a = 1;
var b = 2;
var c = 3;
var d = 4;
var e = 5;

var result = (a + b + c + d + e) / 5;
console.log("BT2 - result: ", result);
// Kết thúc bài tập 2

// Bài tập 3: Quy đổi tiền
/**
 * Đầu vào
 *    đặt tên biến và gán giá trị: var motDollar = 23500;
 * Các bước xử lý
 *    b1: tạo hàm doiDollar()
 *        + tạo biến var soDollar = document.getElementById("soDollar")
 *    b2: cho biến soDollar.value để hiện giá trị
 *    b3: sotienquydoi= sodollarValue * motDollar
 * Đầu ra
 *    sotienquydoi: 47000
 */
var motDollar = 23500;

function doiDollar() {
  var soDollar = document.getElementById("soDollar");
  var sodollarValue = soDollar.value;
  console.log("sotienquydoi: ", sodollarValue * motDollar, "VND");
}
// Kết thúc bài tập 3

// Bài tập 4: Tính diện tích, chu vi hình chữ nhật
/**
 * Đầu vào
 *    gán giá trị cho 2 biến chieuDai và chieuRong
 * Các bước xử lý
 *    công thức: dienTich = chieuDai * chieuRong
 *               chuVi = (chieuDai + chieuRong) * 2
 * Đầu ra
 *    Diện tích:  50
 *    Chu vi:  30
 */
var chieuDai = 10;
var chieuRong = 5;

var dienTich = chieuDai * chieuRong;
console.log("BT4 - Diện tích: ", dienTich);

var chuVi = (chieuDai + chieuRong) * 2;
console.log("BT4 - Chu vi: ", chuVi);
// Kết thúc bài tập 4

// Bài tập 5: Tính tổng 2 ký số
/**
 * Đầu vào
 *    giá của biến: n = 68
 * Các bước xử lý
 *    b1: Lấy hàng đơn vị và hàng chục
 *    b2: tính tổng hàng chục và hàng đơn vị
 * Đầu ra
 *    result = 14
 */
var n = 68;

var hangDonVi = n % 10;
console.log("BT5 - hangDonVi: ", hangDonVi);

var hangChuc = Math.floor(n / 10) % 10;
console.log("BT5 - hangChuc: ", hangChuc);

var result = hangChuc + hangDonVi;
console.log("BT5 - result: ", result);
// Kết thúc bài tập 5
